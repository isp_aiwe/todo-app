FLASK_APP=todo-app.py
FLASK_DEBUG=1
FLASK_ENV=development
DATABASE_URL="postgresql://localhost/todo-app-dev"
SECRET_KEY='secret-key'
SQLALCHEMY_TRACK_MODIFICATIONS=False