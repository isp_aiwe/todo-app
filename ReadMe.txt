Install project

0) You must have installed: python3.*, flask, PostgreSQL 
1) Go to folder, when will be install project
In terminal: 
2) 'cd folder/installing/project'
3) 'git clone https://isp_aiwe@bitbucket.org/isp_aiwe/todo-app.git'
4) 'cd todo-app'
5) 'createdb todo-app-dev'
6) 'pip install -r requirements'
7) 'flask db upgrade'
8) 'flask run'
9) in browser go: 'localhost:5000/login'