from app.models import User
from flask_wtf import FlaskForm
from wtforms.fields import PasswordField, StringField, SubmitField, BooleanField, HiddenField, IntegerField, DateField, SelectField
from wtforms.validators import ValidationError, DataRequired, EqualTo, Email


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign in')


class RegisterForm(FlaskForm):
    email = StringField('Email')


class EditProfileForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    back = SubmitField('Back')
    submit = SubmitField('Submit changes')

    def __init__(self, original_username, original_email, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username
        self.original_email = original_email

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError('Please use a different username')

    def validate_email(self, email):
        if email.data != self.original_email:
            user = User.query.filter_by(email=email.data).first()
            if user is not None:
                raise ValidationError('Please use a different email')


class UserForm(FlaskForm):
    username = StringField('Username')
    email = StringField('Email')
    back = SubmitField('Back')
    edit_profile = SubmitField('Edit profile')


class TodoForm(FlaskForm):
    choises = [
        ('0', 'None'),
        ('1', 'Very low'),
        ('2', 'Low'),
        ('3', 'Medium'),
        ('4', 'High'),
        ('5', 'Critical')
    ]
    title = StringField()
    date = DateField('Due date', format="%m/%d/%Y")
    status = SelectField('Status', choices=choises)
    submit = SubmitField('Create')


class RegisterForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat password', validators=[DataRequired(), EqualTo('password', message="passwords are not the same")])
    submit = SubmitField('Create new user')

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)

    def validate_username(self, username):
        user = User.query.filter_by(username=self.username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email')
