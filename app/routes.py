from app import app, db, bootstrap
from app.forms import UserForm, EditProfileForm, LoginForm, TodoForm, RegisterForm
from app.models import User, Todo
from flask import render_template, url_for, flash, redirect, request
from flask_login import current_user, login_user, logout_user, login_required
import re
from werkzeug.urls import url_parse


@app.route('/')
@app.route('/main')
@login_required
def main():
    form = TodoForm()
    todos = Todo.query.all()
    todos = Todo.query.order_by(Todo.done.asc())
    colors = {1: "darkblue", 2: "aqua", 3: "greenyellow", 4: "yellow", 5: "red"}
    return render_template('main.html', title='Main', form=form, todos=todos, colors=colors)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid password and/or username')
            return redirect(url_for('login'))
        login_user(user)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('main')
        return redirect(next_page)
    return render_template('login.html', title='Sign in', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/user', methods=['GET', 'POST'])
@login_required
def user():
    form = UserForm()
    form.username.data = current_user.username
    form.email.data = current_user.email
    if form.validate_on_submit():
        if re.search(r'back', str(request.form)) is not None:
            return redirect(url_for('main'))
        elif re.search(r'edit_profile', str(request.form)) is not None:
            return redirect(url_for('edit_profile'))
    return render_template('user.html', title='Profile', form=form)


@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username, current_user.email)
    if form.validate_on_submit():
        if re.search(r'submit', str(request.form)) is not None:
            current_user.username = form.username.data
            current_user.email = form.email.data
            db.session.commit()
            flash('You changes has been saved')
        return redirect(url_for('user'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    return render_template('edit_profile.html', title='Edit Profile', form=form)


@app.route('/add_todo', methods=['GET', 'POST'])
@login_required
def add_todo():
    form = TodoForm()
    if form.validate_on_submit():
        todo = Todo(title=form.title.data, date=form.date.data, status=form.status.data, author_id=current_user.id)
        db.session.add(todo)
        db.session.commit()
        flash('Todo task has added to Todo list')
        return redirect(url_for('main'))
    return render_template('edit_todo.html', title="Add Todo", form=form)


@app.route('/edit_todo/<id>', methods=['GET', 'POST', 'DELETE'])
@login_required
def edit_todo(id):
    todo = Todo.query.filter_by(id=id).first_or_404()
    form = TodoForm()
    if form.validate_on_submit():
        todo.title = form.title.data
        todo.date = form.date.data
        todo.status = form.status.data
        db.session.add(todo)
        db.session.commit()
        flash('Todo has been updated')
        return redirect(url_for('main'))
    elif request.method == 'GET':
        form.title.data = todo.title
        form.date.data = todo.date
    return render_template('edit_todo.html', title="Edit Todo", form=form, todo=todo, update=True)


@app.route('/delete_todo/<id>', methods=['GET', 'POST'])
@login_required
def delete_todo(id):
    Todo.query.filter_by(id=id).delete()
    db.session.commit()
    flash('Todo has been succes deleted')
    return redirect(url_for('main'))


@app.route('/todo_done/<id>')
@login_required
def todo_done(id):
    todo = Todo.query.get(id)
    todo.done = False if todo.done else True
    db.session.add(todo)
    db.session.commit()
    return {}


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main'))
    form = RegisterForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500
